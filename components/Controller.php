<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
        
        /**
        funkcija lauj viesim apmeklēt lapu 10 reizes pēc tam liek ielogoties 
        */
//        public function beforeAction($action) {
//        // allowed actions to be performed 
//        $allowed=array(
//            'login','register','passwordreset','updatepassword','logout'
//        );
//        // page views
//        if(!isset(Yii::app()->session['pages'])){
//            Yii::app()->session['pages']=1;
//        }else{
//            Yii::app()->session['pages']=Yii::app()->session['pages']+1;
//        }
//        $pages = Yii::app()->session['pages'];
//        if(Yii::app()->user->isGuest){
//            if($pages>10){
// 
//                if(!in_array($action->id, $allowed)){
//                    $this->redirect(array('/site/login')); // redirect to login
//                }
//            }
//        }   
//        return parent::beforeAction($action);
//    }
}