<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	$this->module->id,
);



$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'ee-demo-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'selectionChanged'=>'function(id){ location.href = "'.$this->createUrl('/tko/default/view').'/id/"+$.fn.yiiGridView.getSelection(id);}',
    'columns'=>array(
        array(
            'id'=>'autoId',
            'class'=>'CCheckBoxColumn',
            'selectableRows' => '50000',
        ),
        array('name'=>'playground_name','type'=>'raw','value'=>'$data->getNameLink()','htmlOptions'=>array("style"=>"width: 250px;"),),
        array('name'=>'playground_size','htmlOptions'=>array("style"=>"width: 120px;"),),
        array(
            'class'=>'CButtonColumn',
            'template'=>'{update}{delete}',
            'buttons'=>array(
                'update'=>array(
                    'url'=>'Yii::app()->createUrl("tko/default/update", array("id"=>$data->id))',
                ),
                'delete'=>array(
                    'url'=>'Yii::app()->createUrl("tko/default/delete", array("id"=>$data->id))',
                )
            )
        ),
    ),
));

//    $this->actionIndex();


?>
