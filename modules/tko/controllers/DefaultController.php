<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{

        $exists = true;
        $RandName = "";
        while ($exists) {
            $RandName = 'Random '.rand(1,20000)."x".rand(1,20000);
            $Count = aaPlaygrounds::model()->count(" playground_name = :x ",array(":x"=>$RandName));
            if ($Count == 0) {
                $exists = false;
            }
        }
        $AddDummyPlayground = new aaPlaygrounds();
        $AddDummyPlayground->playground_name = $RandName;
        $AddDummyPlayground->playground_size = 1;
        if ($AddDummyPlayground->save()) {

        } else {
            print_r($AddDummyPlayground->getErrors());
        }

        $model=new aaPlaygrounds('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['aaPlaygrounds']))
            $model->attributes=$_GET['aaPlaygrounds'];


        $this->render('index',array(
            'model'=>$model,
        ));

	}

    public function actionDemo($id) {




        $model = aaPlaygrounds::model()->findByPk($id);

        $this->render('demoview',array(
            'demoModel'=>$model,
            '_POST'=>$_POST,
        ));

    }

    public function actionAjaxdemo1($id) {


        print_r($_POST);

        echo "
        <script>

        function doAjax2() {
            $('#darbiba').html('".date("Y-m-d H:i:s")." <br>');
        }

        </script>
        ";

    }

}