<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

?>

<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<p>Congratulations! You have successfully created your Yii application - Inroto Playground 1.</p>

<p>You may change the content of this page by modifying the following two files:</p>
<ul>
	Hello world
</ul>

<ul>
	<li>I have played here / Kaspars K.</li>
</ul>

<p>For more details on how to further develop this application, please read
the <a href="http://www.yiiframework.com/doc/">documentation</a>.
Feel free to ask in the <a href="http://www.yiiframework.com/forum/">forum</a>,
should you have any questions.</p>
